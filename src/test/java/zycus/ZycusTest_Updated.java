package zycus;

import org.junit.*;

import io.restassured.RestAssured;
import io.restassured.response.Response;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

import org.junit.FixMethodOrder;

import static es.us.isa.restest.util.FileManager.readFile;
import static org.junit.Assert.fail;
import static org.junit.Assert.assertTrue;

import org.junit.runners.MethodSorters;

import io.qameta.allure.restassured.AllureRestAssured;
import es.us.isa.restest.testcases.restassured.filters.StatusCode5XXFilter;
import es.us.isa.restest.testcases.restassured.filters.NominalOrFaultyTestCaseFilter;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import es.us.isa.restest.testcases.restassured.filters.ResponseValidationFilter;
import es.us.isa.restest.testcases.restassured.filters.CSVFilter;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ZycusTest_Updated {

	private static final String OAI_JSON_URL = "src/test/resources/zycus/swagger_existing.yaml";
	private static final StatusCode5XXFilter statusCode5XXFilter = new StatusCode5XXFilter();
	private static final NominalOrFaultyTestCaseFilter nominalOrFaultyTestCaseFilter = new NominalOrFaultyTestCaseFilter();
	private static final ResponseValidationFilter validationFilter = new ResponseValidationFilter(OAI_JSON_URL);
	private static final AllureRestAssured allureFilter = new AllureRestAssured();
	private static final String APIName = "zycus";
	private static final String testId = "1jyjcix7n3rck";
	private static final CSVFilter csvFilter = new CSVFilter(APIName, testId);
	private String token = "";
	static Map<String,String> cookies = new HashMap<String,String>();;



	@BeforeClass
 	public static void setUp() {
		RestAssured.baseURI = "https://dewdrops-qcvw.zycus.net/";
		statusCode5XXFilter.setAPIName(APIName);
		statusCode5XXFilter.setTestId(testId);
		nominalOrFaultyTestCaseFilter.setAPIName(APIName);
		nominalOrFaultyTestCaseFilter.setTestId(testId);
		validationFilter.setAPIName(APIName);
		validationFilter.setTestId(testId);
		//cookies = new HashMap<String,String>();
	}

	@Test
	public void Aast_login_SETOPERATIONID() {
		String testResultId = "test_login_SETOPERATIONID";
		
		nominalOrFaultyTestCaseFilter.updateFaultyData(false, true, "none");
		statusCode5XXFilter.updateFaultyData(false, true, "none");
		csvFilter.setTestResultId(testResultId);
		statusCode5XXFilter.setTestResultId(testResultId);
		nominalOrFaultyTestCaseFilter.setTestResultId(testResultId);
		validationFilter.setTestResultId(testResultId);
		
		try {
			Response response = RestAssured
					.given().header("Content-Type", "application/json")
					.body("{\n" +
							"\"emailAddress\":\"admin@zycus.com\",\n" +
							"  \"password\": \"Zycus@123\"\n" +
							"}")
					.log().all()
					.filter(allureFilter)
					.filter(statusCode5XXFilter)
					.filter(nominalOrFaultyTestCaseFilter)
					.when()
					.post("/api/u/tms/auth/login");

			response.then().log().all();
			System.out.println("Test passed.");
			cookies = response.cookies();
			//System.out.println(response.cookies());
			token = response.getCookie("SAAS_COMMON_BASE_TOKEN_ID");
			System.setProperty("token1",token);
			System.out.println(cookies);
			System.out.println(token);
		} catch (RuntimeException ex) {
			ex.printStackTrace();
			System.err.println(ex.getMessage());
			fail(ex.getMessage());
		}
		
		
	}

	@Test
	public void test_vb92jv17kcpv_Stopnotification() {
		String testResultId = "test_vb92jv17kcpv_Stopnotification";

		nominalOrFaultyTestCaseFilter.updateFaultyData(false, true, "none");
		statusCode5XXFilter.updateFaultyData(false, true, "none");
		csvFilter.setTestResultId(testResultId);
		statusCode5XXFilter.setTestResultId(testResultId);
		nominalOrFaultyTestCaseFilter.setTestResultId(testResultId);
		validationFilter.setTestResultId(testResultId);

		try {
			String t = System.getProperty("token1");
			Response response = RestAssured
					.given().cookies(cookies)
					.log().all()
					.body("{\"sorts\":[{\"fieldName\":\"date_updated\",\"direction\":\"DESC\"}],\"pageNo\":1,\"perPageRecords\":10}")
					.filter(allureFilter)
					.filter(statusCode5XXFilter)
					.filter(nominalOrFaultyTestCaseFilter)
					.filter(validationFilter)
					.filter(csvFilter)
					.when()
					.post("/cns/api/a/cns/notifications/list");

			response.then().log().all();
			System.out.println("Test passed.");
		} catch (RuntimeException ex) {
			System.err.println(ex.getMessage());
			fail(ex.getMessage());
		}
	}
	@Test
	public void test_sl90a95w865w_Createnotificationtype() {
		String testResultId = "test_sl90a95w865w_Createnotificationtype";
		//delay(1);
		nominalOrFaultyTestCaseFilter.updateFaultyData(false, true, "none");
		statusCode5XXFilter.updateFaultyData(false, true, "none");
		csvFilter.setTestResultId(testResultId);
		statusCode5XXFilter.setTestResultId(testResultId);
		nominalOrFaultyTestCaseFilter.setTestResultId(testResultId);
		validationFilter.setTestResultId(testResultId);

		try {
			ObjectMapper objectMapper = new ObjectMapper();
			String json =  readFile("D:\\Code\\API-RESTTEST\\RestTest_20_JUL\\src\\test\\resources\\zycus_new\\testdata\\cns_api_a_cns_notifications_post_body.json");

			JsonNode jsonBody =  objectMapper.readTree(json);

			Response response = RestAssured
					.given().cookies(cookies)
					.log().all()
					.contentType("application/json")
					.body(jsonBody)
					.filter(allureFilter)
					.filter(statusCode5XXFilter)
					.filter(nominalOrFaultyTestCaseFilter)
					//.filter(validationFilter)
					.filter(csvFilter)
					.when()
					.post("/cns/api/a/cns/notifications");

			response.then().log().all();
			System.out.println("Test passed.");
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RuntimeException ex) {
			System.err.println(ex.getMessage());
			fail(ex.getMessage());
		}
	}
	

}
