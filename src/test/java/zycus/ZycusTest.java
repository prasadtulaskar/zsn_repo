package zycus;

import org.junit.*;

import io.restassured.RestAssured;
import io.restassured.response.Response;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

import org.junit.FixMethodOrder;

import static org.junit.Assert.fail;
import static org.junit.Assert.assertTrue;

import org.junit.runners.MethodSorters;

import io.qameta.allure.restassured.AllureRestAssured;
import es.us.isa.restest.testcases.restassured.filters.StatusCode5XXFilter;
import es.us.isa.restest.testcases.restassured.filters.NominalOrFaultyTestCaseFilter;

import java.io.File;

import es.us.isa.restest.testcases.restassured.filters.ResponseValidationFilter;
import es.us.isa.restest.testcases.restassured.filters.CSVFilter;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ZycusTest {

	private static final String OAI_JSON_URL = "src/test/resources/zycus/swagger_existing.yaml";
	private static final StatusCode5XXFilter statusCode5XXFilter = new StatusCode5XXFilter();
	private static final NominalOrFaultyTestCaseFilter nominalOrFaultyTestCaseFilter = new NominalOrFaultyTestCaseFilter();
	private static final ResponseValidationFilter validationFilter = new ResponseValidationFilter(OAI_JSON_URL);
	private static final AllureRestAssured allureFilter = new AllureRestAssured();
	private static final String APIName = "zycus";
	private static final String testId = "1jyjcix7n3rck";
	private static final CSVFilter csvFilter = new CSVFilter(APIName, testId);
	private String token = "";

	@BeforeClass
 	public static void setUp() {
		RestAssured.baseURI = "https://swagger-ui.zycus.net";
		statusCode5XXFilter.setAPIName(APIName);
		statusCode5XXFilter.setTestId(testId);
		nominalOrFaultyTestCaseFilter.setAPIName(APIName);
		nominalOrFaultyTestCaseFilter.setTestId(testId);
		validationFilter.setAPIName(APIName);
		validationFilter.setTestId(testId);
	}

	@Test
	public void Aast_login_SETOPERATIONID() {
		String testResultId = "test_qxqk7tx2mtdf1_SETOPERATIONID";
		
		nominalOrFaultyTestCaseFilter.updateFaultyData(false, true, "none");
		statusCode5XXFilter.updateFaultyData(false, true, "none");
		csvFilter.setTestResultId(testResultId);
		statusCode5XXFilter.setTestResultId(testResultId);
		nominalOrFaultyTestCaseFilter.setTestResultId(testResultId);
		validationFilter.setTestResultId(testResultId);
		try {
			Response response = RestAssured
					.given().header("Content-Type", "application/json")
					.body("{\n" +
							"\"emailAddress\":\"eircom_supp@zycus.com\",\n" +
							"  \"password\": \"Eproc@1234\"\n" +
							"}")
					.log().all()
					.filter(allureFilter)
					.filter(statusCode5XXFilter)
					.filter(nominalOrFaultyTestCaseFilter)
					.when()
					.post("https://swagger-ui.zycus.net/api/u/tms/auth/login");

			//response.then().log().all();
			System.out.println("Test passed.");
			System.out.println(response.asString());
			token = response.getCookie("SAAS_COMMON_BASE_TOKEN_ID");
			System.setProperty("token1",token);
			System.out.println(token);
		} catch (RuntimeException ex) {
			ex.printStackTrace();
			System.err.println(ex.getMessage());
			fail(ex.getMessage());
		}
	}

	@Test
	public void test_qxqk7tx2mtdf_SETOPERATIONID() {
		String testResultId = "test_qxqk7tx2mtdf_SETOPERATIONID";

		nominalOrFaultyTestCaseFilter.updateFaultyData(false, true, "none");
		statusCode5XXFilter.updateFaultyData(false, true, "none");
		csvFilter.setTestResultId(testResultId);
		statusCode5XXFilter.setTestResultId(testResultId);
		nominalOrFaultyTestCaseFilter.setTestResultId(testResultId);
		validationFilter.setTestResultId(testResultId);

		try {
			String t = System.getProperty("token1");
			Response response = RestAssured
			.given().header("SAAS_COMMON_BASE_TOKEN_ID",t)
				.log().all()
				.filter(allureFilter)
				.filter(statusCode5XXFilter)
				.filter(nominalOrFaultyTestCaseFilter)
				.filter(validationFilter)
				//.filter(csvFilter)
			.when()
				.get("https://swagger-ui.zycus.net/a/cns/notifications/getDismissPref");

			//response.then().log().all();
			System.out.println("Test passed.");
		} catch (RuntimeException ex) {
			ex.printStackTrace();
			System.err.println(ex.getMessage());
			fail(ex.getMessage());
		}
	}

	@Test
	public void test_1iykzv02rmq0j_SETOPERATIONID() {
		String testResultId = "test_1iykzv02rmq0j_SETOPERATIONID";

		nominalOrFaultyTestCaseFilter.updateFaultyData(false, true, "none");
		statusCode5XXFilter.updateFaultyData(false, true, "none");
		csvFilter.setTestResultId(testResultId);
		statusCode5XXFilter.setTestResultId(testResultId);
		nominalOrFaultyTestCaseFilter.setTestResultId(testResultId);
		validationFilter.setTestResultId(testResultId);

		try {
			String t = System.getProperty("token1");
			Response response = RestAssured
			.given().header("SAAS_COMMON_BASE_TOKEN_ID",t)
				.log().all()
				.filter(allureFilter)
				.filter(statusCode5XXFilter)
				.filter(nominalOrFaultyTestCaseFilter)
				//.filter(validationFilter)
				//.filter(csvFilter)
			.when()
				.get("https://swagger-ui.zycus.net/a/cns/notifications/getDismissPref");

			response.then().log().all();
			System.out.println("Test passed.");
		} catch (RuntimeException ex) {
			System.err.println(ex.getMessage());
			fail(ex.getMessage());
		}
	}

	@Test
	public void test_vbbwaajyrfw3_SETOPERATIONID() {
		String testResultId = "test_vbbwaajyrfw3_SETOPERATIONID";

		nominalOrFaultyTestCaseFilter.updateFaultyData(false, true, "none");
		statusCode5XXFilter.updateFaultyData(false, true, "none");
		csvFilter.setTestResultId(testResultId);
		statusCode5XXFilter.setTestResultId(testResultId);
		nominalOrFaultyTestCaseFilter.setTestResultId(testResultId);
		validationFilter.setTestResultId(testResultId);

		try {
			String t = System.getProperty("token1");
			Response response = RestAssured
			.given().header("SAAS_COMMON_BASE_TOKEN_ID",t)
				.log().all()
				.filter(allureFilter)
				.filter(statusCode5XXFilter)
				.filter(nominalOrFaultyTestCaseFilter)
				//.filter(validationFilter)
				//.filter(csvFilter)
			.when()
				.get("https://swagger-ui.zycus.net/a/cns/notifications/getDismissPref");

			response.then().log().all();
			System.out.println("Test passed.");
		} catch (RuntimeException ex) {
			System.err.println(ex.getMessage());
			fail(ex.getMessage());
		}
	}

	@Test
	public void test_urmdzong3aed_SETOPERATIONID() {
		String testResultId = "test_urmdzong3aed_SETOPERATIONID";

		nominalOrFaultyTestCaseFilter.updateFaultyData(false, true, "none");
		statusCode5XXFilter.updateFaultyData(false, true, "none");
		csvFilter.setTestResultId(testResultId);
		statusCode5XXFilter.setTestResultId(testResultId);
		nominalOrFaultyTestCaseFilter.setTestResultId(testResultId);
		validationFilter.setTestResultId(testResultId);

		try {
			String t = System.getProperty("token1");
			Response response = RestAssured
			.given().header("SAAS_COMMON_BASE_TOKEN_ID",t)
				.log().all()
				.filter(allureFilter)
				.filter(statusCode5XXFilter)
				.filter(nominalOrFaultyTestCaseFilter)
				//.filter(validationFilter)
				//.filter(csvFilter)
			.when()
				.get("https://swagger-ui.zycus.net/a/cns/notifications/getDismissPref");

			response.then().log().all();
			System.out.println("Test passed.");
		} catch (RuntimeException ex) {
			System.err.println(ex.getMessage());
			fail(ex.getMessage());
		}
	}

	@Test
	public void test_1if12dhws9s9v_SETOPERATIONID() {
		String testResultId = "test_1if12dhws9s9v_SETOPERATIONID";

		nominalOrFaultyTestCaseFilter.updateFaultyData(false, true, "none");
		statusCode5XXFilter.updateFaultyData(false, true, "none");
		csvFilter.setTestResultId(testResultId);
		statusCode5XXFilter.setTestResultId(testResultId);
		nominalOrFaultyTestCaseFilter.setTestResultId(testResultId);
		validationFilter.setTestResultId(testResultId);

		try {
			String t = System.getProperty("token1");
			Response response = RestAssured
					.given().header("SAAS_COMMON_BASE_TOKEN_ID",t)
				.log().all()
				.filter(allureFilter)
				.filter(statusCode5XXFilter)
				.filter(nominalOrFaultyTestCaseFilter)
				//.filter(validationFilter)
				//.filter(csvFilter)
			.when()
				.get("https://swagger-ui.zycus.net/a/cns/notifications/getDismissPref");

			response.then().log().all();
			System.out.println("Test passed.");
		} catch (RuntimeException ex) {
			System.err.println(ex.getMessage());
			fail(ex.getMessage());
		}
	}
	@Test
	public void test_ubj2dwljni9f_Stopnotification() {
		String testResultId = "test_ubj2dwljni9f_Stopnotification";

		nominalOrFaultyTestCaseFilter.updateFaultyData(false, true, "none");
		statusCode5XXFilter.updateFaultyData(false, true, "none");
		csvFilter.setTestResultId(testResultId);
		statusCode5XXFilter.setTestResultId(testResultId);
		nominalOrFaultyTestCaseFilter.setTestResultId(testResultId);
		validationFilter.setTestResultId(testResultId);

		try {
			Response response = RestAssured
					.given()
					.log().all()
					.pathParam("notification_Id", "birmingham")
					.filter(allureFilter)
					.filter(statusCode5XXFilter)
					.filter(nominalOrFaultyTestCaseFilter)
					.filter(validationFilter)
					.filter(csvFilter)
					.when()
					.post("/a/cns/notifications/{notification_Id}/stop");

			response.then().log().all();
			System.out.println("Test passed.");
		} catch (RuntimeException ex) {
			System.err.println(ex.getMessage());
			fail(ex.getMessage());
		}
	}

	@Test
	public void test_vb92jv17kcpv_Stopnotification() {
		String testResultId = "test_vb92jv17kcpv_Stopnotification";

		nominalOrFaultyTestCaseFilter.updateFaultyData(false, true, "none");
		statusCode5XXFilter.updateFaultyData(false, true, "none");
		csvFilter.setTestResultId(testResultId);
		statusCode5XXFilter.setTestResultId(testResultId);
		nominalOrFaultyTestCaseFilter.setTestResultId(testResultId);
		validationFilter.setTestResultId(testResultId);

		try {
			Response response = RestAssured
					.given()
					.log().all()
					.pathParam("notification_Id", "erection")
					.filter(allureFilter)
					.filter(statusCode5XXFilter)
					.filter(nominalOrFaultyTestCaseFilter)
					.filter(validationFilter)
					.filter(csvFilter)
					.when()
					.post("/a/cns/notifications/{notification_Id}/stop");

			response.then().log().all();
			System.out.println("Test passed.");
		} catch (RuntimeException ex) {
			System.err.println(ex.getMessage());
			fail(ex.getMessage());
		}
	}

	@Test
	public void test_1iv3uf7dq39yp_Stopnotification() {
		String testResultId = "test_1iv3uf7dq39yp_Stopnotification";

		nominalOrFaultyTestCaseFilter.updateFaultyData(false, true, "none");
		statusCode5XXFilter.updateFaultyData(false, true, "none");
		csvFilter.setTestResultId(testResultId);
		statusCode5XXFilter.setTestResultId(testResultId);
		nominalOrFaultyTestCaseFilter.setTestResultId(testResultId);
		validationFilter.setTestResultId(testResultId);

		try {
			Response response = RestAssured
					.given()
					.log().all()
					.pathParam("notification_Id", "croak")
					.filter(allureFilter)
					.filter(statusCode5XXFilter)
					.filter(nominalOrFaultyTestCaseFilter)
					.filter(validationFilter)
					.filter(csvFilter)
					.when()
					.post("/a/cns/notifications/{notification_Id}/stop");

			response.then().log().all();
			System.out.println("Test passed.");
		} catch (RuntimeException ex) {
			System.err.println(ex.getMessage());
			fail(ex.getMessage());
		}
	}

	@Test
	public void test_u7zl505sd3qp_Stopnotification() {
		String testResultId = "test_u7zl505sd3qp_Stopnotification";

		nominalOrFaultyTestCaseFilter.updateFaultyData(false, true, "none");
		statusCode5XXFilter.updateFaultyData(false, true, "none");
		csvFilter.setTestResultId(testResultId);
		statusCode5XXFilter.setTestResultId(testResultId);
		nominalOrFaultyTestCaseFilter.setTestResultId(testResultId);
		validationFilter.setTestResultId(testResultId);

		try {
			Response response = RestAssured
					.given()
					.log().all()
					.pathParam("notification_Id", "inharmoniousness")
					.filter(allureFilter)
					.filter(statusCode5XXFilter)
					.filter(nominalOrFaultyTestCaseFilter)
					.filter(validationFilter)
					.filter(csvFilter)
					.when()
					.post("/a/cns/notifications/{notification_Id}/stop");

			response.then().log().all();
			System.out.println("Test passed.");
		} catch (RuntimeException ex) {
			System.err.println(ex.getMessage());
			fail(ex.getMessage());
		}
	}

	@Test
	public void test_t8cg93jlk7za_Stopnotification() {
		String testResultId = "test_t8cg93jlk7za_Stopnotification";

		nominalOrFaultyTestCaseFilter.updateFaultyData(false, true, "none");
		statusCode5XXFilter.updateFaultyData(false, true, "none");
		csvFilter.setTestResultId(testResultId);
		statusCode5XXFilter.setTestResultId(testResultId);
		nominalOrFaultyTestCaseFilter.setTestResultId(testResultId);
		validationFilter.setTestResultId(testResultId);

		try {
			Response response = RestAssured
					.given()
					.log().all()
					.pathParam("notification_Id", "chaetognath")
					.filter(allureFilter)
					.filter(statusCode5XXFilter)
					.filter(nominalOrFaultyTestCaseFilter)
					.filter(validationFilter)
					.filter(csvFilter)
					.when()
					.post("/a/cns/notifications/{notification_Id}/stop");

			response.then().log().all();
			System.out.println("Test passed.");
		} catch (RuntimeException ex) {
			System.err.println(ex.getMessage());
			fail(ex.getMessage());
		}
	}
	@Test
	public void test_1iavdaxd2e4is_DeleteAttachmentFile() {
		String testResultId = "test_1iavdaxd2e4is_DeleteAttachmentFile";

		nominalOrFaultyTestCaseFilter.updateFaultyData(true, false, "individual_parameter_constraint:Changed value of number parameter attachment_Id from '41' to boolean 'true'");
		statusCode5XXFilter.updateFaultyData(true, false, "individual_parameter_constraint:Changed value of number parameter attachment_Id from '41' to boolean 'true'");
		csvFilter.setTestResultId(testResultId);
		statusCode5XXFilter.setTestResultId(testResultId);
		nominalOrFaultyTestCaseFilter.setTestResultId(testResultId);
		validationFilter.setTestResultId(testResultId);

		try {
			Response response = RestAssured
					.given()
					.log().all()
					.pathParam("attachment_Id", "true")
					.filter(allureFilter)
					.filter(statusCode5XXFilter)
					.filter(nominalOrFaultyTestCaseFilter)
					.filter(validationFilter)
					.filter(csvFilter)
					.when()
					.delete("/a/cns/attachments/{attachment_Id}");

			response.then().log().all();
			System.out.println("Test passed.");
		} catch (RuntimeException ex) {
			System.err.println(ex.getMessage());
			fail(ex.getMessage());
		}
	}

	@Test
	public void test_vf6goqww4602_DeleteAttachmentFile() {
		String testResultId = "test_vf6goqww4602_DeleteAttachmentFile";

		nominalOrFaultyTestCaseFilter.updateFaultyData(true, false, "individual_parameter_constraint:Changed value of number parameter attachment_Id from '94' to string 'zhtlIEeSCEDGSBjivw'");
		statusCode5XXFilter.updateFaultyData(true, false, "individual_parameter_constraint:Changed value of number parameter attachment_Id from '94' to string 'zhtlIEeSCEDGSBjivw'");
		csvFilter.setTestResultId(testResultId);
		statusCode5XXFilter.setTestResultId(testResultId);
		nominalOrFaultyTestCaseFilter.setTestResultId(testResultId);
		validationFilter.setTestResultId(testResultId);

		try {
			Response response = RestAssured
					.given()
					.log().all()
					.pathParam("attachment_Id", "zhtlIEeSCEDGSBjivw")
					.filter(allureFilter)
					.filter(statusCode5XXFilter)
					.filter(nominalOrFaultyTestCaseFilter)
					.filter(validationFilter)
					.filter(csvFilter)
					.when()
					.delete("/a/cns/attachments/{attachment_Id}");

			response.then().log().all();
			System.out.println("Test passed.");
		} catch (RuntimeException ex) {
			System.err.println(ex.getMessage());
			fail(ex.getMessage());
		}
	}

	@Test
	public void test_toiglh3l8300_DeleteAttachmentFile() {
		String testResultId = "test_toiglh3l8300_DeleteAttachmentFile";

		nominalOrFaultyTestCaseFilter.updateFaultyData(false, true, "none");
		statusCode5XXFilter.updateFaultyData(false, true, "none");
		csvFilter.setTestResultId(testResultId);
		statusCode5XXFilter.setTestResultId(testResultId);
		nominalOrFaultyTestCaseFilter.setTestResultId(testResultId);
		validationFilter.setTestResultId(testResultId);

		try {
			Response response = RestAssured
					.given()
					.log().all()
					.pathParam("attachment_Id", "37")
					.filter(allureFilter)
					.filter(statusCode5XXFilter)
					.filter(nominalOrFaultyTestCaseFilter)
					.filter(validationFilter)
					.filter(csvFilter)
					.when()
					.delete("/a/cns/attachments/{attachment_Id}");

			response.then().log().all();
			System.out.println("Test passed.");
		} catch (RuntimeException ex) {
			System.err.println(ex.getMessage());
			fail(ex.getMessage());
		}
	}

	@Test
	public void test_1ji784m71urtv_DeleteAttachmentFile() {
		String testResultId = "test_1ji784m71urtv_DeleteAttachmentFile";

		nominalOrFaultyTestCaseFilter.updateFaultyData(false, true, "none");
		statusCode5XXFilter.updateFaultyData(false, true, "none");
		csvFilter.setTestResultId(testResultId);
		statusCode5XXFilter.setTestResultId(testResultId);
		nominalOrFaultyTestCaseFilter.setTestResultId(testResultId);
		validationFilter.setTestResultId(testResultId);

		try {
			Response response = RestAssured
					.given()
					.log().all()
					.pathParam("attachment_Id", "56")
					.filter(allureFilter)
					.filter(statusCode5XXFilter)
					.filter(nominalOrFaultyTestCaseFilter)
					.filter(validationFilter)
					.filter(csvFilter)
					.when()
					.delete("/a/cns/attachments/{attachment_Id}");

			response.then().log().all();
			System.out.println("Test passed.");
		} catch (RuntimeException ex) {
			System.err.println(ex.getMessage());
			fail(ex.getMessage());
		}
	}

	@Test
	public void test_ts1xrhcd3tra_DeleteAttachmentFile() {
		String testResultId = "test_ts1xrhcd3tra_DeleteAttachmentFile";

		nominalOrFaultyTestCaseFilter.updateFaultyData(false, true, "none");
		statusCode5XXFilter.updateFaultyData(false, true, "none");
		csvFilter.setTestResultId(testResultId);
		statusCode5XXFilter.setTestResultId(testResultId);
		nominalOrFaultyTestCaseFilter.setTestResultId(testResultId);
		validationFilter.setTestResultId(testResultId);

		try {
			Response response = RestAssured
					.given()
					.log().all()
					.pathParam("attachment_Id", "98")
					.filter(allureFilter)
					.filter(statusCode5XXFilter)
					.filter(nominalOrFaultyTestCaseFilter)
					.filter(validationFilter)
					.filter(csvFilter)
					.when()
					.delete("/a/cns/attachments/{attachment_Id}");

			response.then().log().all();
			System.out.println("Test passed.");
		} catch (RuntimeException ex) {
			System.err.println(ex.getMessage());
			fail(ex.getMessage());
		}
	}

}
