package es.us.isa.restest.util;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

import es.us.isa.restest.runners.RESTestRunner;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import static es.us.isa.restest.util.FileManager.createFileIfNotExists;
import static es.us.isa.restest.util.FileManager.deleteFile;

public class CSVManager {

	private static final Logger logger = LogManager.getLogger(CSVManager.class.getName());
	
	/**
	 * Returns a list with the values of the first column in the input CSV file
	 * @param path The path of the CSV file
	 * @return the values of the first column of the CSV file
	 */
	public static List<String> readValues(String path) {
		List<String> values = new ArrayList<String>();
		
		Reader in;
		try {
			
			if(checkJSONInCSVFile(path))
			{
				values = readJSONValuesFromCSVFile(path);
			}
			else
			{
				in = new FileReader(path);
				Iterable<CSVRecord> records = CSVFormat.EXCEL.parse(in);
				for (CSVRecord record : records)
				    values.add(record.get(0));
			}
			

		}catch (IOException ex) {
			logger.error("Error parsing CSV file: {}", path);
			logger.error("Exception: ", ex);
		}
		
		return values;
	}

	public static ArrayList<String> readJSONValuesFromCSVFile(String path)
	{
		String line;
		ArrayList<String> values = new ArrayList<>();
	    
		try
		{
			BufferedReader reader = new BufferedReader(new FileReader(path));
			while ((line = reader.readLine()) != null) 
			{
				if(line != null)
					line= line.trim();
				values.add(line);
			}        
			reader.close();
		
		}catch(Exception e)
		{
			System.out.println("Issue/Problem in reading file -"+ path);
			e.printStackTrace();
			System.exit(-1);
			
		}	
		return values;
	}
	
	public static boolean checkJSONInCSVFile(String path)
	{
		String line;
		boolean isJSON = true;
	    
		try
		{
			BufferedReader reader = new BufferedReader(new FileReader(path));
			while ((line = reader.readLine()) != null) 
			{
				JsonNode jsonData = null;
			    try {
			       
			        ObjectMapper objectMapper = new ObjectMapper();
			        jsonData = objectMapper.readTree(line);
			        
			        if(!(jsonData.isArray() || jsonData.isObject()))
			        {
			        	isJSON = false;
			        }
			        
			        
			    } catch (Exception ex) {
			    	isJSON = false;
			    }
			    return isJSON;
			}
			
			reader.close();
		
		}catch(Exception e)
		{
			System.out.println("Issue/Problem in reading file -"+ path);
			e.printStackTrace();
			System.exit(-1);
			
		}	
		return isJSON;
	}	
		  
	
	
	/**
	 * Returns a list with the values of all rows (including header, if any)
	 * of the input CSV file. Each row is a list of strings (one element per field)
	 * @param path The path of the CSV file
	 * @param delimiter The character that separates the values in each row
	 * @return the values of all rows of the CSV file
	 */
	public static List<List<String>> readCSV(String path, char delimiter) {
		List<List<String>> rows = new ArrayList<>();

		Reader in;
		try {
			in = new FileReader(path);
			Iterable<CSVRecord> records = CSVFormat.EXCEL.withDelimiter(delimiter).parse(in);
			for (CSVRecord record : records) {
				List<String> currentRow = new ArrayList<>();
				for (String field: record)
					currentRow.add(field);
				rows.add(currentRow);
			}
		} catch (IOException ex) {
			logger.error("Error parsing CSV file: {}", path);
			logger.error("Exception: ", ex);
		}

		return rows;
	}

	/**
	 * Returns a list with the values of all rows of the input CSV file. Each row
	 * is a list of strings (one element per field)
	 * @param path The path of the CSV file
	 * @param includeFirstRow Whether to include first row of the CSV in the result
	 *                        or not. Useful for excluding header.
	 * @return the values of all rows of the CSV file
	 */
	public static List<List<String>> readCSV(String path, Boolean includeFirstRow) {
		List<List<String>> rows = readCSV(path, ',');
		if (!includeFirstRow)
			rows.remove(0);
		return rows;
	}

	/**
	 * Call {@link #readCSV(String path, char delimiter)} with delimiter=','
	 * @param path The path of the CSV file
	 * @return the values of all rows of the CSV file
	 */
	public static List<List<String>> readCSV(String path) {
		return readCSV(path, ',');
	}

	/**
	 * Create a new CSV file in the given path and with the given header.
	 * @param path Path where to place the file. Parent folders must be already created
	 * @param header Header to add to the first line. If null, no header will be added
	 */
	public static void createCSVwithHeader(String path, String header) {
		deleteFile(path); // delete file if it exists
		createFileIfNotExists(path);
		writeCSVRow(path, header);
	}
	
	public static void createJSONwritter(String path, String json, String testid) {
		deleteFile(path); // delete file if it exists
		createFileIfNotExists(path);
		writeJSON(path, json, testid);
	}

	public static void writeCSVRow(String path, String row) {
		File csvFile = new File(path);
		try(FileOutputStream oCsvFile = new FileOutputStream(csvFile, true)) {
			row += "\n";
			oCsvFile.write(row.getBytes());
		} catch (IOException e) {
			logger.error("The line could not be written to the CSV: {}", path);
			logger.error("Exception: ", e);
		}

	}
	
	public static void writeJSON(String path, String json, String testid) {
		BufferedWriter writer = null ;
		try {
			writer = new BufferedWriter(new FileWriter(path, true));
			writer.append(System.lineSeparator());
			writer.append("Start");
		    writer.append(testid);
		    writer.append(System.lineSeparator());
		    writer.append(json);
		    writer.append("end");
		    writer.append(System.lineSeparator());
		    writer.close();
		} catch (IOException e) {
			logger.error("The line could not be written to the JSON: {}", testid);
			logger.error("Exception: ", e);
		} finally{
			try {
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}
}
