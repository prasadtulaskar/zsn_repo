package es.us.isa.restest.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.tools.JavaCompiler;
import javax.tools.JavaFileObject;
import javax.tools.StandardJavaFileManager;
import javax.tools.StandardLocation;
import javax.tools.ToolProvider;

/**
 * 
 * @author Sergio Segura
 */
public class ClassLoader {

	private static final Logger logger = LogManager.getLogger(ClassLoader.class.getName());

	public static Class<?> loadClass(String filePath, String className) {
		File sourceFile = new File(filePath);
		Class<?> loadedClass= null;
		
		List<String> optionList = new ArrayList<String>();
		
		if(!File.separator.equals("\\"))
		{	
			try
			{
				File currentDirFile = new File(".");
				String currentDir = currentDirFile.getCanonicalPath();
				// set compiler's classpath to be same as the runtime's
				String projectBuildDir = currentDir+File.separator+"target";
				String projectFinalDir = "restest-1.0.1-SNAPSHOT"; 
				String jarPath = projectBuildDir+File.separator+projectFinalDir+".lib/*";
				String classesDir = projectBuildDir+File.separator+"classes";
				optionList.addAll(Arrays.asList("-classpath", System.getProperty("java.class.path") + File.separator + buildClassPath(jarPath)));
				System.setProperty("java.class.path", System.getProperty("java.class.path") + File.pathSeparator + classesDir + File.pathSeparator+ buildClassPath(jarPath));
				System.out.println(System.getProperty("java.class.path"));
				
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			
		}

		// Compile the source file 
		try {
		JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
		StandardJavaFileManager fileManager = compiler.getStandardFileManager(null, null, null);
		File parentDirectory = sourceFile.getParentFile();
		fileManager.setLocation(StandardLocation.CLASS_OUTPUT, Arrays.asList(parentDirectory));
		Iterable<? extends JavaFileObject> compilationUnits = fileManager.getJavaFileObjectsFromFiles(Arrays.asList(sourceFile));
		compiler.getTask(null, fileManager, null, optionList, null, compilationUnits).call();
		fileManager.close();
		
		 // load the compiled class
		 loadedClass = loadClass(parentDirectory, className);

		} catch (IOException e) {
			logger.error("Error loading class");
			logger.error("Exception: ", e);
		} catch (NullPointerException e) {
			logger.error("Error loading class. Make sure JDK is used");
			logger.error("Exception: ", e);
		}
		
		return loadedClass;
	}

	private static Class<?> loadClass(File parentDirectory, String className) {
		Class<?> loadedClass= null;
		try(URLClassLoader classLoader = URLClassLoader.newInstance(new URL[] { parentDirectory.toURI().toURL() })) {
			loadedClass = classLoader.loadClass(className);
		} catch (IOException e) {
			logger.error("Error loading class");
			logger.error("Exception: ", e);
		} catch (ClassNotFoundException e) {
			logger.error("Class not found");
			logger.error("Exception: ", e);
		}

		return loadedClass;
	}
	
	private static String buildClassPath(String... paths) {
        StringBuilder sb = new StringBuilder();
        for (String path : paths) {
            if (path.endsWith("*")) {
                path = path.substring(0, path.length() - 1);
                File pathFile = new File(path);
                for (File file : pathFile.listFiles()) {
                    if (file.isFile() && file.getName().endsWith(".jar")) {
                        sb.append(path);
                        sb.append(file.getName());
                        sb.append(System.getProperty("path.separator"));
                    }
                }
            } else {
                sb.append(path);
                sb.append(System.getProperty("path.separator"));
            }
        }
        return sb.toString();
    }


}
