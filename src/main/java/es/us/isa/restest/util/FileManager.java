package es.us.isa.restest.util;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class FileManager {

    private static Logger logger = LogManager.getLogger(FileManager.class.getName());

    public static Boolean createFileIfNotExists(String path) {
        File file = new File(path);
        try {
            return file.createNewFile();
        } catch (IOException e) {
            logger.error("Exception: ", e);
        }
        return null;
    }

    public static Boolean checkIfExists(String path) {
        
    	if(path == null || path.equals("null"))
    		return false;
    	
    	File file = new File(path);
        
        return file.exists();
        
    }

    public static Boolean deleteFile(String path) {
        File file = new File(path);
        return file.delete();
    }

    // Create target dir if it does not exist
    public static Boolean createDir(String targetDir) {
        if (targetDir.charAt(targetDir.length()-1) != '/')
            targetDir += "/";
        File dir = new File(targetDir);
        return dir.mkdirs();
    }

    public static void deleteDir(String path) {
        File file = new File(path);
        try {
            FileUtils.deleteDirectory(file);
        } catch (IOException e) {
            logger.error("Error deleting dir ");
            logger.error("Exception: ", e);
        }
    }

    public static String readFile(String path) {
        try {
            return new String(Files.readAllBytes(Paths.get(path)));
        } catch (IOException e) {
            logger.error("Exception: ", e);
        }
        return null;
    }

    public static void writeFile(String path,String data) {
        try {
            FileUtils.write(new File(path),data);
        } catch (IOException e) {
            logger.error("Exception: ", e);
        }
    }

    public static boolean getXLSValue(Xls_Reader xls, String url) {
        String SheetName = "Config";
        try {
            int r = xls.getRowCount(SheetName);
            for(int i=2;i<=r;i++){
                if(xls.getCellData(SheetName,"Endpoint",i).equalsIgnoreCase(url)){
                    if(xls.getCellData(SheetName,"Execute",i).equalsIgnoreCase("Y")){
                        return true;
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Exception: ", e);
        }
        return false;
    }

    public static void delay(Integer time) {
        try {
            logger.info("Introducing delay of {} seconds", time);
            TimeUnit.SECONDS.sleep(time);
        } catch (InterruptedException e) {
            logger.error("Error introducing delay", e);
            logger.error(e.getMessage());
            Thread.currentThread().interrupt();
        }
    }

    public static List<File> getFileNames(String folderPath){
        List<File> javaFlesInFolder = new ArrayList<File>();
        try {
            List<File> filesInFolder = Files.walk(Paths.get(folderPath))
                    .filter(Files::isRegularFile)
                    .map(Path::toFile)
                    .collect(Collectors.toList());
            for(int i=0;i<filesInFolder.size();i++){
                if(filesInFolder.get(i).getName().contains("java"))
                    javaFlesInFolder.add(filesInFolder.get(i));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return javaFlesInFolder;
    }

}
