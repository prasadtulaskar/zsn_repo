package es.us.isa.restest.util;

import java.util.ArrayList;

public class FieldCoverage 
{
	private String fieldName;
	private String fieldType;
	
	private ArrayList<String> testData = new ArrayList<>();
	private int actualTestCaseCount;
	private int expectedTestCaseCount;
	
	
	public String getFieldType() {
		return fieldType;
	}
	public void setFieldType(String fieldType) {
		this.fieldType = fieldType;
	}
	public String getFieldName() {
		return fieldName;
	}
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}
	public ArrayList<String> getTestData() {
		return testData;
	}
	public void setTestData(ArrayList<String> testData) {
		this.testData = testData;
	}
	public int getActualTestCaseCount() {
		return actualTestCaseCount;
	}
	public void setActualTestCaseCount(int actualTestCaseCount) {
		this.actualTestCaseCount = actualTestCaseCount;
	}
	public int getExpectedTestCaseCount() {
		return expectedTestCaseCount;
	}
	public void setExpectedTestCaseCount(int expectedTestCaseCount) {
		this.expectedTestCaseCount = expectedTestCaseCount;
	}

}
