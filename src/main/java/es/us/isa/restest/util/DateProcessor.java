package es.us.isa.restest.util;

import java.util.Calendar;

public class DateProcessor {
	
	public static long getFutureDateInMilliseconds()
	{
		
		return getFutureDateInMilliseconds(1);
	}
	
	public static long getFutureDateInMilliseconds(int days)
	{
		
		Calendar todayCal = Calendar.getInstance();
 		todayCal.add(Calendar.DATE, days);
 		return todayCal.getTime().getTime();
 		
	}
	
	public static long getFutureDateInMilliseconds(String sDays)
	{
		return getFutureDateInMilliseconds(Integer.parseInt(sDays));
		
	}
	
	public static long getTodayDateInMilliseconds()
	{
		
		Calendar todayCal = Calendar.getInstance();
 		return todayCal.getTime().getTime();
 		
	}
	
	public static long getPastDateInMilliseconds()
	{
		
		return getFutureDateInMilliseconds(-1);
	}
	
	public static long getPastDateInMilliseconds(int days)
	{
		
		Calendar todayCal = Calendar.getInstance();
 		todayCal.add(Calendar.DATE, days);
 		return todayCal.getTime().getTime();
 		
	}

}
